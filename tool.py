import json
from dataclasses import dataclass
import tkinter as tk
from tkinter import ttk

pokaz = "Pokaż odpowiedź!"
znalem = "Znałem odpowiedź!"
nie_wiedzialem = "Nie wiedziałem!"


@dataclass
class Question:
    question: str
    correct_answer: str
    example_sentence: str = None
    status: str = "Nowe"


@dataclass
class AppStatus:
    title = "Aplikacja do nauki słówek"
    size = "800x600"
    font = ("Arial", 18)
    dictionary_file = "data.json"
    question_label: tk.Label = None
    answer_label: tk.Label = None
    show_answer_button: tk.Button = None
    know_answer_button: tk.Button = None
    dont_know_button: tk.Button = None
    current_qestion: Question = None
    _questions: list[Question] = None
    main_window: tk.Tk = None
    progress_bar: ttk.Progressbar = None

    def get_questions(self):
        return self._questions

    def get_percentage_of_known_words(self):
        all_known = len([question for question in self._questions if question.status == znalem])
        return round(all_known / len(self._questions) * 100, 2)

    def __init__(self):
        with open(self.dictionary_file, encoding='utf-8') as file:
            data = json.load(file)
        self._questions = [Question(data[row]["słówko"], data[row]["tłumaczenie"], data[row]["zdanie przykładowe"])
                           for row in data]


def data_converter():
    app_status = AppStatus()
    data = {}
    for question in app_status.get_questions():
        data[question.question] = {
            "słówko": question.question,
            "tłumaczenie": question.correct_answer,
            "zdanie przykładowe": None
        }
    return data


def dump_json(file_name: str, data: dict) -> None:
    """
    example:
        from tool import data_converter, dump_json
        a = data_converter()
        print(a)
        dump_json("data.json", a)
    :param file_name:
    :param data:
    :return:
    """
    with open(file_name, "w", encoding="utf-8") as file:
        json.dump(data, file, indent=4, ensure_ascii=False)
