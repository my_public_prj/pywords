import random
import tkinter as tk
from tkinter import ttk

from tool import AppStatus, Question, znalem, nie_wiedzialem, pokaz

app_status = AppStatus()


def generate_question(questions: list[Question]) -> Question:
    """
    Generate a random question and update the question label.
    Args:
        questions (list[Question]): The list of questions to choose from.
    Returns:
        Question: The randomly selected question.
    """

    unknown_questions = [question for question in questions if question.status != znalem]
    question: Question = random.choice(unknown_questions)
    app_status.question_label.config(text=question.question)  # Update the question label with the selected question
    app_status.know_answer_button.config(state=tk.DISABLED)
    app_status.dont_know_button.config(state=tk.DISABLED)
    return question


def reset_question(questions: list[Question]) -> None:
    """
    Resets the question and answer labels.
    Args:
        questions (list[Question]): The list of questions to choose from.
    """
    app_status.question_label.config(text="")
    # Ustawienie wartości paska postępu
    app_status.progress_bar['value'] = app_status.get_percentage_of_known_words()
    app_status.progress_bar.update()
    if app_status.progress_bar['value'] == 100:
        app_status.main_window.quit()
    app_status.current_qestion = generate_question(questions)
    app_status.answer_label.config(text="")
    app_status.show_answer_button.config(state=tk.NORMAL)
    app_status.know_answer_button.config(state=tk.DISABLED)
    app_status.dont_know_button.config(state=tk.DISABLED)


def show_answer(question: Question):
    """
    Updates the text of the answer label with the correct answer of the given question.
    Args:
        question (Question): The question object containing the correct answer.
    """
    app_status.answer_label.config(text=question.correct_answer)
    app_status.show_answer_button.config(state=tk.DISABLED)
    app_status.know_answer_button.config(state=tk.NORMAL)
    app_status.dont_know_button.config(state=tk.NORMAL)


# Funkcja do oceniania znajomości
def grade_knowledge(question: Question, user_answer):
    if user_answer == znalem:
        question.status = znalem
    elif user_answer == nie_wiedzialem:
        question.status = nie_wiedzialem
    reset_question(app_status.get_questions())


def update_label_width(event: tk.Event, window: tk.Tk) -> None:
    """
    Update the width of the labels based on the window width.
    Args:
        event (tk.Event): The event that triggered the function.
        window (tk.Tk): The main window.
    Returns:
        None
    """
    window_width = window.winfo_width()

    # Check if the text in the labels is still aligned
    if app_status.question_label.winfo_width() <= window_width and app_status.answer_label.winfo_width() <= window_width:
        # Update the wraplength value for the question label
        app_status.question_label.configure(wraplength=window_width - 20)
        # Update the wraplength value for the answer label
        app_status.answer_label.configure(wraplength=window_width - 20)
    else:
        # Resize the window to trigger a new event
        window.geometry(window.geometry())


def main():
    # Tworzenie okna aplikacji
    window = tk.Tk()
    window.title(app_status.title)
    window.geometry(app_status.size)
    window.update()

    app_status.main_window = window
    app_status.main_window.bind_all("<Control-q>", lambda event: app_status.main_window.quit())
    app_status.main_window.bind_all("<Alt-s>", lambda event: show_answer(app_status.current_qestion))
    app_status.main_window.bind_all("z", lambda event: app_status.know_answer_button.cget(
        "state") == tk.NORMAL and grade_knowledge(app_status.current_qestion, znalem))
    app_status.main_window.bind_all("Z", lambda event: app_status.know_answer_button.cget(
        "state") == tk.NORMAL and grade_knowledge(app_status.current_qestion, znalem))
    app_status.main_window.bind_all("n", lambda event: app_status.dont_know_button.cget(
        "state") == tk.NORMAL and grade_knowledge(app_status.current_qestion, nie_wiedzialem))
    app_status.main_window.bind_all("N", lambda event: app_status.dont_know_button.cget(
        "state") == tk.NORMAL and grade_knowledge(app_status.current_qestion, nie_wiedzialem))

    # Tworzenie nieedytowalnego pola tekstowego dla pytania
    question_label = tk.Label(window, text="", relief=tk.SUNKEN, padx=10, pady=10,
                              justify=tk.CENTER, width=0, wraplength=window.winfo_width() - 20, height=3,
                              font=app_status.font)
    question_label.pack(fill=tk.X)
    app_status.question_label = question_label

    # Tworzenie nieedytowalnego pola tekstowego dla odpowiedzi
    answer_label = tk.Label(window, text="", relief=tk.SUNKEN, padx=10, pady=10, justify=tk.CENTER,
                            width=0, wraplength=window.winfo_width() - 20, height=3,
                            font=app_status.font)
    answer_label.pack(fill=tk.X)
    app_status.answer_label = answer_label

    # Tworzenie przycisku "Pokaż odpowiedź"
    show_answer_button = tk.Button(window, text=pokaz,
                                   command=lambda: show_answer(app_status.current_qestion), font=app_status.font)

    show_answer_button.pack(pady=10)
    app_status.show_answer_button = show_answer_button

    # Tworzenie kontenera Frame
    frame = tk.Frame(window)
    frame.pack()

    # Tworzenie przycisku "Znałem odpowiedź"
    know_answer_button = tk.Button(frame, text=znalem,
                                   command=lambda: grade_knowledge(app_status.current_qestion, znalem),
                                   font=app_status.font)
    know_answer_button.pack(pady=10)
    app_status.know_answer_button = know_answer_button

    # Tworzenie przycisku "Nie wiedziałem"
    dont_know_button = tk.Button(frame, text=nie_wiedzialem,
                                 command=lambda: grade_knowledge(app_status.current_qestion, nie_wiedzialem),
                                 font=app_status.font)
    dont_know_button.pack(pady=10)
    app_status.dont_know_button = dont_know_button

    # Umieszczenie przycisków w kontenerze Frame
    know_answer_button.pack(side=tk.LEFT, padx=5)
    dont_know_button.pack(side=tk.LEFT, padx=5)

    # Wyświetlenie pierwszego pytania automatycznie po uruchomieniu aplikacji
    app_status.current_qestion = generate_question(app_status.get_questions())

    # Tworzenie paska stanu
    status_bar = tk.Label(window, text="", bd=1, relief=tk.SUNKEN, anchor=tk.W)
    status_bar.pack(side=tk.BOTTOM, fill=tk.X)
    # Tworzenie paska postępu
    app_status.progress_bar = ttk.Progressbar(status_bar, length=window.winfo_width(), mode='determinate')
    app_status.progress_bar.pack(fill=tk.X)

    # Reakcja na zmianę szerokości okna
    window.bind("<Configure>", lambda event: update_label_width(event, window))

    # Uruchamianie aplikacji
    window.mainloop()


if __name__ == "__main__":
    main()
